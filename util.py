import numpy as np
from nltk.tokenize.punkt import PunktSentenceTokenizer as Segmenter
from gensim.models import KeyedVectors
from gensim.utils import simple_preprocess
from discord import TextChannel, DMChannel, GroupChannel, Forbidden
import unicodedata as ud
from typing import List

segmenter = Segmenter()
segmented = segmenter.sentences_from_text

sanitize = simple_preprocess
#def sanitize(s):
#    s = ud.normalize('NFD', s)
#    excl = 'MP'
#    def nsp(c):
#        return ud.category(c)[0] not in excl
#    
#    return ''.join(filter(nsp, s)).lower().split()

def vectorize(W: KeyedVectors, S: List[str]) -> np.ndarray:
    S = [s for s in S if s in W.vocab.keys()]
    if len(S) == 0:
        return None
    V = np.average(np.array([W[s] for s in S]), 0)
    return V

def chname(ch: TextChannel) -> str:
    def uname(u):
        return f'@{u.name}#{u.discriminator}'
    if isinstance(ch, DMChannel):
        name = uname(ch.recipient)
    elif isinstance(ch, GroupChannel):
        name = ', '.join(map(uname, ch.recipients))
    else:
        name = f'{ch.guild.name}#{ch.name}'
    return name

async def unspool(channels: List[TextChannel],
                  maxdata: int,
                  onProgress=(lambda _: None)) \
                  -> List[List[str]]:
    D = [[] for _ in range(len(channels))]
    c = len(channels)
    for i, ch in enumerate(channels):
        try:
            s = 0
            async for m in ch.history():
                D[i].append(sanitize(m.clean_content))
                s += len(m.clean_content)
                onProgress((i+1)/c + s/maxdata)
                if s >= (maxdata/c):
                    break
            onProgress((i+1)/c)
        except Forbidden:
            pass
    return D