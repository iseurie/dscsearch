from asyncio import get_event_loop
from sys import stdout
from os import getenv
from gensim.models import KeyedVectors
from sys import stderr
import warnings
from util import *

warnings.filterwarnings('ignore')

def summarize(R: range):
    global C, M, W
    names = []
    pools = []
    for i in R:
        ch = C[i]
        tags = ''
        if M[i] is not None:
            tags = (w for w, _ in W.similar_by_vector(M[i], 8)) 
            tags = ' '.join(f'#{s}' for s in tags)
        pools.append(tags)
        name = chname(ch)
        names.append(name)
    p = max(map(len, names)) + 4
    for i in range(len(names)):
        padding = ' '*(p - len(names[i]))
        stdout.write(f'{names[i]}{padding}{pools[i]}\n')

from discord import Client
class MClient(Client):
    async def on_ready(self):
        from gensim.models.word2vec import Word2Vec
        # from discord import TextChannel
        from itertools import chain
        global C, M, W

        C = list(filter(
            lambda ch: isinstance(ch, TextChannel),
            chain(self.get_all_channels(), self.private_channels)))

        def pb(width, progress):
            fill = int(progress*width)
            empty = width - fill
            bar = '#'*fill + ' '*empty
            return f'[{bar}]'

        def progressCb(progress):
            bar = pb(30, progress)
            stdout.write(f'\rRetrieving data: {bar} ({progress*100:00.01f}%)...')
            
        stdout.write('Retrieving data...')
        sz = 10 * len(C) * (0x01<<20) # retrieve at most 10MB of data per channel
        minsz = 0x01 << 10 # set minimum length at 1KB
        # list of list of lists of token lists
        D = await unspool(C, sz, progressCb)
        S = [len(' '.join(chain(*S))) for S in D]
        # filter indices for our floor length
        R = [i for i in range(len(C)) if S[i] >= minsz]
        # update vectors 
        C = [C[i] for i in R]
        D = [D[i] for i in R]
        print('\n...Indexing...')
        W = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300-SLIM.bin', binary=True)
        M = np.array([vectorize(W, chain(*S)) for S in D])
        await self.logout()


dCl = MClient()
token = getenv('DC_UTOKEN')
get_event_loop().run_until_complete(dCl.start(token, bot=False))
print('...Ready.')

while True:
    try:
        Q = input('> ')
        if Q == ':quit' or Q == ':q':
            raise KeyboardInterrupt()
    except KeyboardInterrupt:
        exit()
    Q = sanitize(Q)
    Q = vectorize(W, Q)

    # cosine similarity of the semantic vector of the given channel index to
    # the query's
    def ranking(i):
        if M[i] is None:
            return 0
        else:
            return np.dot(M[i], Q) / np.linalg.norm(M[i])

    R = [i for i in range(len(C))]
    R.sort(key=ranking)
    # print the top eight channels, their names (and guilds, if applicable),
    # and their top eight keywords
    summarize(R[:8])